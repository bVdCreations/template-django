from celery_example.tasks import power, task_to_celery_django, task_to_worker_1, task_to_worker_1_result


def test_tasks():
    result = power.delay(6)
    assert result.get() == 64


def test_task_celery_django():
    task_to_celery_django()


def test_task_to_worker_1():
    task_to_worker_1()


def test_task_to_worker_1_result():
    result = task_to_worker_1_result()
    print('stuck')
    assert result.get() == 'worker'
