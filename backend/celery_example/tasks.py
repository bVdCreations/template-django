from backend.celery import app


@app.task
def power(n):
    """Return 2 to the n'th power"""
    return 2 ** n


def task_to_celery_django():
    app.send_task('django.tasks.custom_task_name', kwargs={'sum': [5, 7, 8]})


@app.task(name='django.tasks.custom_task_name')
def custom_task_name(**kwargs):
    return kwargs['sum']


def task_to_worker_1():
    app.send_task('data.process.custom_worker_task_name', kwargs={'sum': [5, 7, 8]})


def task_to_worker_1_result():
    worker_task = app.signature('data.process.custom_worker_task_name', kwargs={'sum': [5, 7, 8]}).delay()
    return worker_task
