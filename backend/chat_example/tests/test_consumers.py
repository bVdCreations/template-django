import pytest
from channels.testing import WebsocketCommunicator
# from chat_example.consumers import ChatConsumer
from backend.routing import application


@pytest.mark.asyncio
async def test_chat_consumer():
    communicator = WebsocketCommunicator(application, "ws/chat/testchat/")
    connected, subprotocol = await communicator.connect()
    assert connected
    # Test sending text
    await communicator.send_json_to({"message": "hello"})
    response = await communicator.receive_json_from()
    print()
    assert response["message"] == "hello"
    # Close
    await communicator.disconnect()
