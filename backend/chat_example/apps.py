from django.apps import AppConfig


class ChatExampleConfig(AppConfig):
    name = 'chat_example'
