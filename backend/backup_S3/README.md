# backup data base to AWS S3 bucket

This django application backes up the django database and stores the backup inside an AWS S3 bucket

## requirements

- django
- [django-dbbackup](https://django-dbbackup.readthedocs.io/en/stable/index.html)
- [django-storages](https://django-storages.readthedocs.io/en/latest/)
- celery
- [boto3](https://boto3.amazonaws.com/v1/documentation/api/latest/index.html)

## setings

the specific setting related to this application can be found at `backend/settings/backup_S3.py`

as well the create an db-backup.env file in the root folder with the following content

```bash
DJANGO-BACKUP-AWS-ACCESS-KEY=aws-acces-key
DJANGO-BACKUP-AWS-SECRET-KEY=aws-secret-key
DJANGO-BACKUP-BUCKET-NAME=bucket-name
```

The tasks are periodically called with celery-beat.  
This is run by the docker service 'celery_beat'
