from django.apps import AppConfig


class BackupS3Config(AppConfig):
    name = 'backup_S3'
