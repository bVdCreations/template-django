from django.contrib.auth import get_user_model

from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.generics import (
    RetrieveAPIView,
    CreateAPIView,
)
from rest_framework.permissions import AllowAny, IsAdminUser

from .services import token_authentication
from .serializers import (
    TokenSerializer,
    UserSerializer
)

User = get_user_model()


class LoginAPIView(APIView):

    permission_classes = [AllowAny]

    def post(self, request, *args, **kwargs):
        token = token_authentication(request)
        return Response(TokenSerializer(token).data, status=status.HTTP_202_ACCEPTED)


class RetrieveMeAPIView(RetrieveAPIView):

    serializer_class = UserSerializer

    def retrieve(self, request, *args, **kwargs):
        instance = request.user
        serializer = self.get_serializer(instance)

        return Response(serializer.data)


class CreateUserAPIView(CreateAPIView):

    serializer_class = UserSerializer
    permission_classes = [IsAdminUser]
