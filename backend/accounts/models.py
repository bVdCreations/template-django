# from django.db import models
from django.contrib.auth.models import AbstractUser

# Create your models here.

# Extending User Model Using a Custom Model Extending AbstractUser
# https://simpleisbetterthancomplex.com/tutorial/2016/07/22/how-to-extend-django-user-model.html#abstractuser


class User (AbstractUser):
    """
    Extended User Model
    """
    # birth_date = models.DateField(null=True, blank=True)

    def __str__(self):
        return self.username
