from django.urls import path

from .views import (
    LoginAPIView,
    RetrieveMeAPIView,
    CreateUserAPIView,
    )

app_name = 'accounts'

urlpatterns = [
    path('login/', LoginAPIView.as_view(), name='login'),
    path('me/', RetrieveMeAPIView.as_view(), name='me'),
    path('user/', CreateUserAPIView.as_view(), name='user'),
]
