import pytest

from ...validators import unicode_username_validator
from rest_framework import serializers

invalid_message = "Enter a valid username. This value may contain only letters, numbers, and @/./+/-/_ characters."


def test_valid_unicode_username_validator():
    unicode_username_validator('Valid_value_@.+-_1234567890')


@pytest.mark.parametrize("value", [
    '^', '#', '/'
])
def test_unvalid_unicode_username_validator(value):

    with pytest.raises(serializers.ValidationError) as errormessage:
        unicode_username_validator(value)
    assert invalid_message in str(errormessage.value)
