from unittest.mock import patch, MagicMock

import pytest

from django.contrib.auth import get_user_model
from rest_framework.authtoken.models import Token

from ...selectors import (
    retrieve_user_by_kwargs,
    check_user_password,
    get_or_create_token
)

User = get_user_model()


class TestUserSelectors():

    @pytest.mark.django_db
    @pytest.mark.parametrize("kwargs", [
        {'username': 'BasicTestUser', 'email': 'basictestuser@uniqueemail.com'},
        {'email': 'basictestuser@uniqueemail.com'},
        {'username': 'BasicTestUser'},
    ])
    def test_retrieve_user_by_email_or_username(self, kwargs, user):
        retrieved_user = retrieve_user_by_kwargs(**kwargs)
        assert isinstance(retrieved_user, User)
        assert retrieved_user.id == user.id

    @pytest.mark.django_db
    def test_retrieve_user_by_email_or_username_no_user(self):

        invalid_message = 'user and password does not match'

        with pytest.raises(Exception) as errormessage:

            retrieve_user_by_kwargs(**{'email': 'doesnotexits'})

        assert invalid_message in str(errormessage.value)

    @pytest.mark.django_db
    @pytest.mark.parametrize("kwargs", [
        {'username': 'BasicTestUser', 'email': 'wrong@uniqueemail.com'},
        {'username': 'wrongTestUser', 'email': 'basictestuser@uniqueemail.com'}
    ])
    def test_retrieve_user_by_email_or_username_wrong_combo(self, kwargs, user):

        invalid_message = 'user and password does not match'

        with pytest.raises(Exception) as errormessage:

            retrieve_user_by_kwargs(**kwargs)

        assert invalid_message in str(errormessage.value)


class TestCheckUserPassword():

    def test_correct_password(self):
        password = 'superSecretPassword'

        user = MagicMock()
        user.check_password.return_value = True

        check_user_password(user, password)

        user.check_password.assert_called_once_with(password)

    def test_incorrect_password(self):
        password = 'wrongpass'
        invalid_message = 'user and password does not match'

        user = MagicMock()
        user.check_password.return_value = False

        with pytest.raises(Exception) as errormessage:

            check_user_password(user, password)

        assert invalid_message in str(errormessage.value)

        user.check_password.assert_called_once_with(password)

    @pytest.mark.django_db
    def test_correct_password_db(self, user):
        password = 'superSecretPassword'
        check_user_password(user, password)

    @pytest.mark.django_db
    def test_incorrect_password_db(self, user):
        password = 'wrongpass'
        invalid_message = 'user and password does not match'

        with pytest.raises(Exception) as errormessage:

            check_user_password(user, password)

        assert invalid_message in str(errormessage.value)


class TestCreateToken():

    @patch('accounts.selectors.Token.objects.get_or_create', return_value=('fake token obj', True))
    def test_get_or_create_token(self, mock_token, user_unsaved):

        get_or_create_token(user_unsaved)

        mock_token.assert_called_once_with(user=user_unsaved)

    @pytest.mark.django_db
    def test_get_or_create_token_db(self, user):
        token = get_or_create_token(user)

        assert isinstance(token, Token)
        assert token.user_id == user.id

    @pytest.mark.django_db
    def test_create_double_token(self, user):
        token1 = get_or_create_token(user)
        token2 = get_or_create_token(user)

        assert token1.key == token2.key
