import pytest

from unittest.mock import MagicMock
from rest_framework.test import APIRequestFactory

from ... import services

factory = APIRequestFactory()


@pytest.fixture(scope='class')
def login_request():
    post_data = {'password': 'dummypassd', 'email': 'test@test.com', 'username': 'dummyuser'}
    login_request = factory.post('/accounts/login', post_data, format='json')
    login_request = MagicMock(wraps=login_request)
    login_request.data = post_data

    return login_request


class TestTokenAuthentication():

    def test_validate_request_data(self, login_request):

        input_data = login_request.data

        validated_data = services.validate_request_data(login_request)

        assert len(input_data.keys()) == len(validated_data.keys())
        assert 'password' in validated_data.keys()
        assert 'username' in validated_data.keys()
        assert 'email' in validated_data.keys()
