import pytest
from django.urls import reverse
from django.contrib.auth import get_user_model

User = get_user_model()


@pytest.mark.django_db()
class TestLoginAPIView():

    URL = reverse('accounts:login')
    post_data = {
        'password': 'superSecretPassword',
        'email': 'basictestuser@uniqueemail.com',
        'username': 'BasicTestUser'}

    def test_url_endpoint(self, client):

        response = client.post(self.URL, self.post_data)

        assert response.status_code == 202

    def test_responce_data(self, client):

        response = client.post(self.URL, self.post_data)

        assert len(response.data.keys())
        assert 'auth_token' in response.data.keys()

    @pytest.mark.parametrize("post_data", [
        {'username': 'BasicTestUser', 'email': 'wrong@uniqueemail.com',
            'password': 'superSecretPassword'},
        {'username': 'wrongTestUser', 'email': 'basictestuser@uniqueemail.com',
            'password': 'superSecretPassword'}
    ])
    def test_wrong_user(self, post_data, client):

        response = client.post(self.URL, post_data)

        assert response.status_code == 404

    def test_wrong_password(self, client):

        post_data = {
            'password': 'wrongPassword',
            'email': 'basictestuser@uniqueemail.com',
            'username': 'BasicTestUser'}

        response = client.post(self.URL, post_data)

        assert response.status_code == 404

    @pytest.mark.parametrize("post_data", [
        {'username': 'BasicTestUser', 'password': 'superSecretPassword'},
        {'email': 'basictestuser@uniqueemail.com',
            'password': 'superSecretPassword'}
    ])
    def test_username_or_email(self, post_data, client):

        response = client.post(self.URL, post_data)

        assert response.status_code == 202


@pytest.mark.django_db()
class TestRetrieveMeAPIView():

    URL = reverse('accounts:me')

    def test_endpoint(self, client, user):

        client.force_authenticate(user=user)
        response = client.get(self.URL)

        assert response.status_code == 200

    def test_responce_data(self, client, user):

        client.force_authenticate(user=user)
        response = client.get(self.URL)

        assert len(response.data.keys()) == 2
        assert "username" in response.data.keys()
        assert "email" in response.data.keys()
        assert response.data.get("username") == user.username
        assert response.data.get("email") == user.email

    def test_unauth(self, client):

        client.force_authenticate(user=None)
        response = client.get(self.URL)

        assert response.status_code == 401


@pytest.mark.django_db()
class TestCreateUserAPIView():

    post_data = {
        'password': 'superSecretPassword',
        'email': 'createuser@test.com',
        'username': 'CreateTestUser'}

    URL = reverse('accounts:user')

    def test_endpoint(self, client, admin):

        client.force_authenticate(user=admin)
        response = client.post(self.URL, self.post_data)

        assert response.status_code == 201
        assert User.objects.filter(username='CreateTestUser').exists()

    def test_responce_data(self, client, admin):
        client.force_authenticate(user=admin)
        response = client.post(self.URL, self.post_data)

        assert len(response.data.keys()) == 2
        assert 'username' in response.data.keys()
        assert 'email' in response.data.keys()
        assert response.data['username'] == self.post_data['username']
        assert response.data['email'] == self.post_data['email']

    def test_unauth(self, client, user):
        client.force_authenticate(user=user)
        response = client.post(self.URL, self.post_data)

        assert response.status_code == 403
        assert not User.objects.filter(username='CreateTestUser').exists()
