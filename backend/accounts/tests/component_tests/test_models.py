from django.contrib.auth import get_user_model


User = get_user_model()


class TestUserModel():

    def test_str(self, user_unsaved):
        userstr = str(user_unsaved)

        assert userstr == 'UnsavedTestUser'
