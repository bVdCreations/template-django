import pytest

from rest_framework import serializers
from rest_framework.authtoken.models import Token

from ...serializers import (
    LoginSerializer,
    TokenSerializer,
    UserSerializer
    )


class TestLoginSerializer():

    def test_serialized_data(self):
        dummy_data = {'password': 'dummypassd',
                      'email': 'test@test.com', 'username': 'dummyuser'}
        serializer = LoginSerializer(data=dummy_data)
        valid = serializer.is_valid(raise_exception=True)
        assert valid is True
        assert serializer.validated_data['password'] == dummy_data['password']
        assert serializer.validated_data['email'] == dummy_data['email']
        assert serializer.validated_data['username'] == dummy_data['username']

    @pytest.mark.parametrize("dummy_username,expected", [
        ('Valid_username@.+-_', True),
        ('', False),
        ('shdayeh/', False),
        ('long_username_' + ('a' * 30), False)
    ])
    def test_serialized_unvalid_username(self, dummy_username, expected):
        dummy_data = {'password': 'dummy_password',
                      'email': 'test@test.com', 'username': dummy_username}
        serializer = LoginSerializer(data=dummy_data)
        valid = serializer.is_valid()
        assert valid == expected

    @pytest.mark.parametrize("dummy_email,expected", [
        ('test@email.test', True),
        ('test@noemail', False),
        ('noemail', False),
        ('no.at', False),
        ('', False),
        ('dubble@point.co.uk', True)
    ])
    def test_serialized_unvalid_email(self, dummy_email, expected):
        dummy_data = {'password': 'dummy_password',
                      'email': dummy_email, 'username': 'dummy_username'}
        serializer = LoginSerializer(data=dummy_data)
        valid = serializer.is_valid()
        assert valid == expected

    @pytest.mark.parametrize("dummy_password,expected", [
        ('Valid_password@.+-_', True),
        ('', False),
        ('7char67', False),
        ('long_password_' + ('a' * 24), False)
    ])
    def test_serialized_unvalid_password(self, dummy_password, expected):
        dummy_data = {'password': dummy_password,
                      'email': 'test@test.com', 'username': 'dummy_username'}
        serializer = LoginSerializer(data=dummy_data)
        valid = serializer.is_valid()
        assert valid == expected

    @pytest.mark.parametrize("dummy_data,expected", [
        ({'password': 'dummy_password', 'email': 'test@test.com',
          'username': 'dummy_username'}, True),
        ({'email': 'test@test.com', 'username': 'dummy_username'}, False),
        ({'password': 'dummy_password', 'username': 'dummy_username'}, True),
        ({'password': 'dummy_password', 'email': 'test@test.com'}, True),
        ({'password': 'dummy_password'}, False),
    ])
    def test_input_data(self, dummy_data, expected):
        serializer = LoginSerializer(data=dummy_data)
        valid = serializer.is_valid()
        assert valid == expected

    def test_unvalid_message_validator_email(self):

        value = 'unvalid.email'
        dummy_data = {'password': 'dummy_password',
                      'email': value, 'username': 'dummy_username'}

        invalid_message = 'Enter a valid email address.'

        with pytest.raises(serializers.ValidationError) as errormessage:
            serializer = LoginSerializer(data=dummy_data)
            serializer.is_valid(raise_exception=True)
        assert invalid_message in str(errormessage.value)

    def test_unvalid_message_validator_username_toshort(self):

        value = 'ab'
        dummy_data = {'password': 'dummy_password',
                      'email': 'test@test.com', 'username': value}

        invalid_message = 'Ensure this field has at least 3 characters.'

        with pytest.raises(serializers.ValidationError) as errormessage:
            serializer = LoginSerializer(data=dummy_data)
            serializer.is_valid(raise_exception=True)
        assert invalid_message in str(errormessage.value)

    def test_unvalid_message_validator_username_tolong(self):

        value = 'a' * 25
        dummy_data = {'password': 'dummy_password',
                      'email': 'test@test.com', 'username': value}

        invalid_message = 'Ensure this field has no more than 24 characters.'

        with pytest.raises(serializers.ValidationError) as errormessage:
            serializer = LoginSerializer(data=dummy_data)
            serializer.is_valid(raise_exception=True)
        assert invalid_message in str(errormessage.value)

    def test_unvalid_message_validator_username_no_unicode(self):

        value = 'nounicodechar_%'
        dummy_data = {'password': 'dummy_password',
                      'email': 'test@test.com', 'username': value}

        invalid_message = 'Enter a valid username. ' + (
            'This value may contain only letters, numbers, and @/./+/-/_ characters.')

        with pytest.raises(serializers.ValidationError) as errormessage:
            serializer = LoginSerializer(data=dummy_data)
            serializer.is_valid(raise_exception=True)
        assert invalid_message in str(errormessage.value)

    def test_unvalid_message_validator_password_toshort(self):

        value = 'ab'
        dummy_data = {'password': value,
                      'email': 'test@test.com', 'username': 'dummy_username'}

        invalid_message = 'Ensure this field has at least 8 characters.'

        with pytest.raises(serializers.ValidationError) as errormessage:
            serializer = LoginSerializer(data=dummy_data)
            serializer.is_valid(raise_exception=True)
        assert invalid_message in str(errormessage.value)

    def test_unvalid_message_validator_password_tolong(self):

        value = 'a' * 25
        dummy_data = {'password': value,
                      'email': 'test@test.com', 'username': 'dummy_username'}

        invalid_message = 'Ensure this field has no more than 24 characters.'

        with pytest.raises(serializers.ValidationError) as errormessage:
            serializer = LoginSerializer(data=dummy_data)
            serializer.is_valid(raise_exception=True)
        assert invalid_message in str(errormessage.value)

    def test_unvalid_input_message_validator_no_password(self):

        dummy_data = {'email': 'test@test.com', 'username': 'dummy_username'}

        invalid_message = 'This field is required.'

        with pytest.raises(serializers.ValidationError) as errormessage:
            serializer = LoginSerializer(data=dummy_data)
            serializer.is_valid(raise_exception=True)
        assert invalid_message in str(errormessage.value)

    def test_unvalid_input_message_validator_no_email_or_username(self):

        dummy_data = {'password': 'dummy_password'}

        invalid_message = 'Provide an username or email.'

        with pytest.raises(serializers.ValidationError) as errormessage:
            serializer = LoginSerializer(data=dummy_data)
            serializer.is_valid(raise_exception=True)
        assert invalid_message in str(errormessage.value)


class TestTokenSerializer():

    @pytest.mark.django_db
    def test_output_data(self, user):
        token, _ = Token.objects.get_or_create(user=user)
        tokendata = TokenSerializer(token).data

        assert len(dict(tokendata).keys()) == 1
        assert 'auth_token' in dict(tokendata).keys()
        auth_token_value = tokendata.get('auth_token')
        assert isinstance(auth_token_value, str)
        assert len(auth_token_value) == 40
        assert " " not in auth_token_value


class TestUserSerializer():

    @pytest.mark.django_db
    def test_output_data(self, user):

        userdata = UserSerializer(user).data

        assert len(dict(userdata).keys()) == 2
        assert 'username' in dict(userdata).keys()
        assert 'email' in dict(userdata).keys()
