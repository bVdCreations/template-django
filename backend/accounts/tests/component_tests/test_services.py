from unittest.mock import MagicMock

import pytest

from rest_framework.test import APIRequestFactory
from rest_framework.authtoken.models import Token

from ...services import token_authentication

factory = APIRequestFactory()


@pytest.fixture()
def login_request():
    post_data = {
        'password': 'superSecretPassword',
        'email': 'basictestuser@uniqueemail.com',
        'username': 'BasicTestUser'}
    login_request = factory.post('/accounts/login', post_data, format='json')
    login_request = MagicMock(wraps=login_request)
    login_request.data = post_data

    return login_request


@pytest.mark.django_db
def test_token_authentication(login_request, user):

    token = token_authentication(login_request)

    assert isinstance(token, Token)
    assert token.user_id == user.id
