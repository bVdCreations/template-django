from django.contrib.auth import get_user_model

from rest_framework.exceptions import NotFound
from rest_framework.authtoken.models import Token

User = get_user_model()

ERRORMESSAGE = {'user_password': 'user and password does not match'}


def retrieve_user_by_kwargs(**kwargs) -> User:

    user = User.objects.filter(**kwargs).first()
    if user is None:
        raise NotFound(ERRORMESSAGE['user_password'])
    return user


def check_user_password(user: User, password: str):
    correct_password = user.check_password(password)
    if not correct_password:
        raise NotFound(ERRORMESSAGE['user_password'])


def get_or_create_token(user: User) -> Token:

    token, _ = Token.objects.get_or_create(user=user)

    return token
