from rest_framework.authtoken.models import Token

from .serializers import LoginSerializer
from .selectors import retrieve_user_by_kwargs, check_user_password, get_or_create_token


def token_authentication(request) -> Token:

    validated_data = validate_request_data(request)
    raw_password = validated_data.pop('password')

    user = retrieve_user_by_kwargs(**validated_data)
    check_user_password(user, raw_password)

    return get_or_create_token(user=user)


def validate_request_data(request) -> dict:

    serializer = LoginSerializer(data=request.data)
    serializer.is_valid()

    return dict(serializer.validated_data)
