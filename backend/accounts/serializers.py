from django.contrib.auth import get_user_model

from rest_framework import serializers
from rest_framework.authtoken.models import Token

from .validators import usernameValidatorlist

User = get_user_model()


def password_field(**kwargs):
    return serializers.CharField(
        min_length=8,
        max_length=24,
        style={"input_type": "password"},
        **kwargs
    )


def email_field(**kwargs):
    return serializers.EmailField(**kwargs)


def username_field(**kwargs):
    return serializers.CharField(
        min_length=3,
        max_length=24,
        validators=usernameValidatorlist,
        **kwargs
    )


class TokenSerializer(serializers.ModelSerializer):
    auth_token = serializers.CharField(source="key")

    class Meta:
        model = Token
        fields = ("auth_token",)


class LoginSerializer(serializers.Serializer):

    password = password_field(required=True)
    email = email_field(required=False)
    username = username_field(required=False)

    def validate(self, data):
        """
        check if username or email are provided
        """

        email = data.get('email', None)
        username = data.get('username', None)

        no_email_or_username_given = (email is None) and (username is None)

        if no_email_or_username_given:
            raise serializers.ValidationError("Provide an username or email.")
        return data


class UserSerializer(serializers.ModelSerializer):

    password = password_field(
        required=True, write_only=True)
    email = email_field(required=True)
    username = username_field(required=True)

    class Meta:
        model = User
        fields = ("username", "email", 'password')
