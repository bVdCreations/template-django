from django.contrib.auth.validators import UnicodeUsernameValidator
from django.core.exceptions import ValidationError

from rest_framework import serializers


def unicode_username_validator(value):
    """wraps django validator with a serializers.ValidationError"""
    try:
        UnicodeUsernameValidator()(value)
    except ValidationError as e:
        raise serializers.ValidationError(e)


usernameValidatorlist = [unicode_username_validator]
