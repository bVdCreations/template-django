from django.db.utils import ProgrammingError
from django.core.management.base import BaseCommand, CommandError

from django.db import connection


class Command(BaseCommand):

    help = 'create a testing database'

    default_options = {'db_name': "postgres_test",
                       'db_owner': "postgres",  'db_encoding': 'UTF8'}

    def add_arguments(self, parser):
        parser.add_argument('--db_alias', type=str)
        parser.add_argument('--db_name', type=str)
        parser.add_argument('--db_owner', type=str)
        parser.add_argument('--db_encoding', type=str)

    def handle(self, *args, **options):

        filtered_options = self.parse_options(**options)
        try:
            self.execute_sql(**filtered_options)
        except ProgrammingError as e:
            raise CommandError(e)

    def execute_sql(self, **kwargs):

        with connection.cursor() as cursor:
            cursor.execute(f"DROP DATABASE IF EXISTS {kwargs['db_name']};")
            cursor.execute(self.query(**kwargs))

    def parse_options(self, **kwargs):

        options = dict(self.default_options)
        for key, value in kwargs.items():
            save_option = (value is not None) and (
                key in self.default_options.items())
            if save_option:
                options[key] = value

        return options

    def query(self, db_name: str = "test_postgres", db_owner: str = "postgres", db_encoding: str = 'UTF8'):
        query = f"""

        CREATE DATABASE {db_name}
            WITH
            OWNER = {db_owner}
            ENCODING = {db_encoding}
        ;

        """

        return query
