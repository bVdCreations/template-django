import pytest
from django.contrib.auth import get_user_model
from django.core.cache import cache


@pytest.fixture(scope='function')
def clear_cache(django_db_blocker, client):

    print('\nCREATE FUNCTION FIXTURE : clear cache')
    yield cache.clear()
    print('\nTEARDOWN FUNCTION FIXTURE : clear cache')


@pytest.fixture(scope="session")
def client():
    from rest_framework.test import APIClient
    print('\nCREATE SESSION FIXTURE : client')
    return APIClient()


@pytest.fixture(scope='session')
def user(django_db_blocker):
    with django_db_blocker.unblock():

        print('\nCREATE SESSION FIXTURE : user')
        User = get_user_model()
        if User.objects.filter(username='BasicTestUser').exists():
            user = User.objects.get(username='BasicTestUser')
        else:
            user = User.objects.create_user(
                'BasicTestUser', 'basictestuser@uniqueemail.com', 'superSecretPassword')
            user.save()
        yield user
        print('\nTEARDOWN SESSION FIXTURE : user')
        # user.delete()


@pytest.fixture(scope='session')
def admin(django_db_blocker):
    with django_db_blocker.unblock():

        print('\nCREATE SESSION FIXTURE : admin')
        User = get_user_model()
        if User.objects.filter(username='AdminUser').exists():
            user = User.objects.get(username='AdminUser')
        else:
            user = User.objects.create_user(
                username='AdminUser',
                email='adminuser@uniqueemail.com',
                password='superSecretPassword',
                is_superuser=True,
                is_staff=True)
            user.save()
        yield user
        print('\nTEARDOWN SESSION FIXTURE : admin')
        # user.delete()


@pytest.fixture(scope='session')
def user_unsaved():
    print('\nCREATE SESSION FIXTURE : user_unsaved \n')
    User = get_user_model()
    user = User(
        username='UnsavedTestUser',
        email='Unsavedtestuser@uniqueemail.com',
        password='superSecretPassword'
    )
    yield user
    print('\nTEARDOWN SESSION FIXTURE : user_unsaved')
    del user


@pytest.fixture(scope='session')
def django_db_setup():
    # this function needs to run for the test of the database to work
    print('SETUP DATABASE')
