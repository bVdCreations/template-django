import time

from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page

from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import permissions


class CachePostView(APIView):

    permission_classes = (permissions.AllowAny,)

    # Cache page for the requested url
    @method_decorator(cache_page(60*60*2))
    def get(self, request, format=None):
        content = {
            'title': 'Post title',
            'body': 'Post content'
        }

        # wait for test purposes
        time.sleep(0.1)

        return Response(content)
