from django.urls import path

from .views import CachePostView

app_name = 'example_cache'

urlpatterns = [
    path('users/me/groups', CachePostView.as_view(), name='cache'),
]
