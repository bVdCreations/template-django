from django.urls import reverse

import time


class TestCache():

    URL = reverse('example_cache:cache')

    def test_url_endpoint(self, client):

        response = client.get(self.URL)

        assert response.status_code == 200

    def test_cache(self, client, clear_cache):
        before_no_cache = time.time()
        client.get(self.URL)
        after_no_cache = time.time()

        before_cache = time.time()
        client.get(self.URL)
        after_cache = time.time()
        no_cache_timer = after_no_cache - before_no_cache
        cache_timer = after_cache - before_cache
        assert no_cache_timer > 0.1
        assert cache_timer < 0.1
