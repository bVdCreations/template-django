import os
from .common import *  # noqa : E401,E403

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = os.environ.get(
    'DJANGO_SECRET_KEY')

ALLOWED_HOSTS = ['domain website']
