import os

from celery.schedules import crontab


BACKUP_S3_APPS = (
    "dbbackup",
    "backup_S3"
)

# DBBACKUP_STORAGE = "django.core.files.storage.FileSystemStorage"
# DBBACKUP_STORAGE_OPTIONS = {
#     "location": "/usr/src/app/backend/backup"
# }

DBBACKUP_STORAGE = "storages.backends.s3boto3.S3Boto3Storage"
DBBACKUP_STORAGE_OPTIONS = {
    "access_key": os.environ.get("DJANGO-BACKUP-AWS-ACCESS-KEY", ""),
    "secret_key": os.environ.get("DJANGO-BACKUP-AWS-SECRET-KEY", ""),
    "bucket_name": os.environ.get("DJANGO-BACKUP-BUCKET-NAME", "")
}

AWS_DEFAULT_ACL = None

CELERY_BEAT_BACKUP_SCHEDULE = {
    "backup": {
        "task": "backup_S3.tasks.backup",
        # Executes every Monday morning at 7:30 a.m.
        "schedule": crontab(hour=7, minute=30, day_of_week=1),
    },
}
