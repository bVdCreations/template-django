
from .develop import *  # noqa : E401,E403

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': "postgres_test",
        'USER': "postgres",
        'HOST': 'db',
        'PORT': 5432,  # default postgres port
        'PASSWORD': 'noSecretDevPassword'
    }
}
