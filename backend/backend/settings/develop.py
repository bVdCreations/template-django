import os
from .common import *  # noqa : E401,E403

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = os.environ.get(
    'DJANGO_SECRET_KEY', ')y!wp31nq)-v2q&y*_dwpuuothal)fmcqpnz*rbqcn1%4e@%&b')

DEBUG = True

ALLOWED_HOSTS = ['0.0.0.0', 'localhost', 'testserver']
