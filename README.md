# Template Django

## About

This has as goal to be a template for starting my django projects
The template is used:

- To test new functionallty
- To document specific use cases as example for other projects
- As basis to start new projects

## Requirments

This project is only tested on MAC and LINUX,
for windows there will be problems with the makefiles

requirements:

- docker
- docker-compose

## Features

This project is still a **work in progress**

- [x] Django
- [x] Docker
- [x] Docker-compose
- [x] Rest (Django rest framework)
- [x] Testings (pytest)
- [x] Token Auth
- [x] Django Celery
- [x] Celery without Django
- [ ] Logging
- [x] Caching
- [x] Channels
- [x] Nginx
- [ ] CI/CD
- [ ] Project doc sphinx
- [ ] Api doc (swagger)
- [ ] Production and stage ready
- [ ] Docker Swarm
- [ ] ...

## Setup

at the start of a new project rename the docker images names in the following files:

- docker-compose.development.yml
- docker-compose.production.yml

Add the correct static builders inside docker nginx

- nginx/dockerfile

### Development

To setup the development environment on your local computer.
Run the following command

```bash
make init-dev
```

This will:

- build all the docker containers
- run the containers
- setup the testing database
- create all the database tables
- run all the tests

### Stage

In progress

### Production

In progress
