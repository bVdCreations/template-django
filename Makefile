DOCKER_DEV = -f docker-compose.production.yml -f docker-compose.development.yml

init-dev:
	# initialize the local develop environment
	sh ./scripts/init/init_develop.sh

dev:
	# start develop environment
	docker-compose $(DOCKER_DEV) up ${ARGS}

dev-d:
	# start develop environment in the deamon
	ARGS=-d make dev

dev-build:
	# build docker images of the develop environment
	docker-compose $(DOCKER_DEV) build ${ARGS}

dev-build-clean:
	# build docker images of the develop environment with no cache
	ARGS=--no-cache make dev-build

dev-stop:
	# stop containers develop environment
	docker-compose $(DOCKER_DEV) stop

prod:
	docker-compose -f docker-compose.production.yml up

bash-backend:
	docker exec -it backend bash

bash-db:
	docker exec -it postgres bash -c "su postgres"

pip-freeze:
	docker exec -it backend bash -c "pip freeze > requirements.txt"

shell-backend:
	docker exec -it backend bash -c "python manage.py shell_plus"

db-migrate:
	docker exec -it backend bash -c "python manage.py makemigrations && python manage.py migrate"
	make test-db-migrate

test-db-recreate:
	docker exec -it backend bash -c "python manage.py create_test_db"

test-db-migrate:
	docker exec -it backend bash -c "python manage.py migrate --settings=backend.settings.test_develop"

test:
	docker exec -it backend bash -c "pytest ${APP} -s -m'not django_db'"

test-all:
	docker exec -it backend bash -c "pytest ${APP} -s"

test-accounts:
	APP=accounts make test

test-accounts-all:
	APP=accounts make test-all

test-celery:
	APP=celery_app make test

test-db-celery:
	APP=celery_app make test-db

cov:
	docker exec -it backend bash -c "pytest --cov --cov-config=.coveragerc --cov-report html"