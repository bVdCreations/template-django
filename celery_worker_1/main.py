import importlib
from celery import Celery
from core.settings import Config

app = Celery('tasks')
app.config_from_object(Config)

# to prevent unfound module errors at autodiscover_tasks import all task_modules before
for module in Config.task_modules:
    importlib.import_module(module)

app.autodiscover_tasks(Config.task_modules)
