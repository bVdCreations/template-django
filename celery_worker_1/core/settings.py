
import os


class Config:
    broker_url = os.environ.get('DJANGO_CELERY_BROKER_URL', 'amqp://rabbit:my_password@rabbitmq:5672/')
    result_backend = 'rpc://'
    task_modules = ['app_module']  # custom settings: define modules with tasks
