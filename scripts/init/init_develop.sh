#!/bin/bash

echo 'initialize the local develop enviroment'

echo 'requirement : \n
        - docker
        - docker-compose
    '

echo 'all requirements are met \n'

echo 'building docker images for development \n\n'
    make dev-build
echo 'docker images build \n'

echo 'run develop container in the background \n'
    make dev-d
echo 'containers are running \n'

echo 'wait for 5 seconds for the database to startup completely'
    sleep 5

echo 'create database for the testing setup \n'
    make test-db-recreate
echo 'test database created \n'

echo 'run django migrations \n'
    make db-migrate
echo 'migrations done \n'

echo 'test instalation'
    make test-all
