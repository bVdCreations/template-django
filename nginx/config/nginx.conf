user nginx nginx;

pid /run/nginx.pid;

error_log /var/log/nginx/nginx.error.log;

events {
    worker_connections 1024;
    accept_mutex off;
}


http{

    include "/etc/nginx/mime.types";

    # octet or bit stream (download instead of display) 
    default_type application/octet-stream;

    sendfile on;

    # Hide Nginx Server Version
    server_tokens off;

    # Expires map
    map $sent_http_content_type $expires {
    default                    off;
    text/html                  epoch;
    text/css                   epoch;
    application/javascript     epoch;
    ~image/                    max;
    }

    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                    '$status $body_bytes_sent "$http_referer" '
                    '"$http_user_agent" "$http_x_forwarded_for"';

    upstream django_server {
        include /etc/nginx/upstream_backend.conf;
    }

    # redirect port 80 to 443 in production only
    server {
        include /etc/nginx/redirect.conf;
    }


    # now we declare our main server
    server {

        # ssl configurations
        include /etc/nginx/ssl.conf;

        client_max_body_size 4G;
        server_name _;

        # Expiration map connection
        expires $expires;

        gzip on;
        gzip_disable "msie6";
        gzip_vary on;
        gzip_proxied any;
        gzip_comp_level 6;
        gzip_buffers 16 8k;
        gzip_http_version 1.1;
        gzip_types application/javascript text/plain text/css application/json application/x-javascript text/xml application/xml application/xml+rss text/javascript;

        keepalive_timeout 5;

        root /usr/share/nginx/html;

        
        # location /test{
        #     return 200 "it works";
        # }

        location /admin{
            access_log /var/log/nginx/acces.admin.log;

            try_files $uri @backend;
        }

        location /api {
            access_log /var/log/nginx/acces.api.log;

            try_files $uri @backend;
        }

        location / {
            access_log /var/log/nginx/acces.frontend.log;
            # setting for frondend single aplication
            try_files $uri $uri/ /index.html;
        }

        location ~* \.(?:ico|css|js|gif|jpe?g|png)$ {

            # Some basic cache-control for static files to be sent to the browser
            expires max;
            add_header Pragma public;
            add_header Cache-Control "public, must-revalidate, proxy-revalidate";
        }

        location @backend {
            # everything is passed to Gunicorn
            
            proxy_pass http://django_server;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header Host $host;
            proxy_redirect off;
        }

    }

}